using System;
using System.Linq;
using System.Reflection;

namespace UnityDI
{
    internal class ReflectionFactory
    {
        private readonly UnityDi _unityDi;

        public ReflectionFactory(UnityDi unityDi)
        {
            _unityDi = unityDi;
        }

        public object Create (RegisteredType registeredType)
        {
            var constructorInfo = GetSingleConstructorOf(registeredType.Type);
            var instance = HasParameterlessConstructor(constructorInfo) ? Activator.CreateInstance(registeredType.Type) : Activator.CreateInstance(registeredType.Type, ResolveParameters(constructorInfo));

            if (registeredType.IsSingleton)
            {
                registeredType.SetInstance(instance);
            }

            return instance;
        }

        private bool HasParameterlessConstructor(ConstructorInfo constructor)
        {
            var parameterList = constructor.GetParameters();
            return parameterList.Length == 0;
        }

        private ConstructorInfo GetSingleConstructorOf(Type type)
        {
            return type.GetConstructors().Single();
        }

        private object[] ResolveParameters(ConstructorInfo constructor)
        {
            var parameters = constructor.GetParameters();
            return parameters.Select(parameter => _unityDi.Resolve(parameter.ParameterType)).ToArray();
        }
    }
}