﻿using System;
using System.Collections.Generic;

namespace UnityDI
{
    public class UnityDi
    {
        private Dictionary<Type, RegisteredType> _registry;
        private readonly ReflectionFactory _reflectionFactory;

        public UnityDi ()
        {
            _registry = new Dictionary<Type, RegisteredType>();
            _reflectionFactory = new ReflectionFactory(this);
        }

        public RegisteredType Register <TInterface, TImplementation>()
        {
            Validate.ThatTypeHasNotBeenRegisteredYet(typeof(TInterface), _registry);
            Validate.ThatHasSingleConstructor(typeof(TImplementation));

            var registeredType = new RegisteredType(typeof(TImplementation));
            _registry.Add(typeof(TInterface), registeredType);
            return registeredType;
        }

        public void Unregister <T> ()
        {
            Unregister(typeof(T));
        }

        private void Unregister(Type givenType)
        {
            Validate.ThatTypeIsRegistered(givenType, _registry);
            _registry.Remove(givenType);
        }

        public T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }

        internal object Resolve (Type givenType)
        {
            Validate.ThatTypeIsRegistered(givenType, _registry);
            var registerdType = _registry[givenType];

            if (registerdType.IsSingleton && registerdType.HasInstance)
            {
                return registerdType.Instance;
            }

            return _reflectionFactory.Create(registerdType);
        }

    }
}
