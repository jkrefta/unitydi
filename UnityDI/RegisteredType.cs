﻿using System;

namespace UnityDI
{
    public class RegisteredType
    {
        public Type Type;
        public bool DoNotDispose { get; private set; }
        public bool IsSingleton { get; private set; }
        public object Instance { get; private set; }
        public bool HasInstance => Instance != null;

        public RegisteredType(Type type)
        {
            Type = type;
        }

        public RegisteredType SetLifeCycleSingle ()
        {
            IsSingleton = true;
            return this;
        }

        public RegisteredType SetInstance (object instance)
        {
            ValidateThatGivenObjectIsCompliantWithRegisterdType(instance);
            EnsureThatSingletonHasOnlyOneInstance();
            Instance = instance;
            return this;
        }

        public RegisteredType SetDisposedManualy()
        {
            DoNotDispose = true;
            return this;
        }

        private void EnsureThatSingletonHasOnlyOneInstance()
        {
            if (HasInstance && IsSingleton)
            {
                throw new ArgumentException(string.Format("{0} is set to singleton and therefore you cannot set new instance", Type));
            }
        }

        private void ValidateThatGivenObjectIsCompliantWithRegisterdType(object instance)
        {
            if (Type.IsInstanceOfType(instance) == false)
            {
                throw new ArgumentException(string.Format("{0} is not compliant with {1}", instance.GetType(), Type));
            }
        }
    }
}
