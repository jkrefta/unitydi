﻿using System;
using System.Collections.Generic;

namespace UnityDI
{
    internal class Validate
    {
        public static void ThatTypeHasNotBeenRegisteredYet(Type type, Dictionary<Type, RegisteredType> registry)
        {
            if (registry.ContainsKey(type))
            {
                throw new NotSupportedException("UnityDI does not support registration of more than one implementation per type");
            }
        }

        public static void ThatTypeIsRegistered(Type type, Dictionary<Type, RegisteredType> registry)
        {
            if (registry.ContainsKey(type))
            {
                return;
            }
            throw new ArgumentException(string.Format("Cannot resolve type {0}. Have you forgotten to register it ?", type.Name));
        }

        public static void ThatHasSingleConstructor(Type type)
        {
            var constructors = type.GetConstructors();
            if (constructors.Length == 1)
            {
                return;
            }
            throw new NotSupportedException(string.Format("UnityDI can only handle types with single constructor. Type {0} has {1}", type.Name, constructors.Length));
        }
    }
}
