namespace UnityDI.Tests.TestDummy
{
    public class ParametrizedDummy : IDummy
    {
        public IParameter Parameter;

        public ParametrizedDummy (IParameter parameter)
        {
            Parameter = parameter;
        }

        public string Get()
        {
            return Parameter.Get();
        }
    }
}