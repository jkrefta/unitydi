namespace UnityDI.Tests.TestDummy
{
    public interface IParameter
    {
        string Get();
    }
}