namespace UnityDI.Tests.TestDummy
{
    public class Parameter : IParameter
    {
        public string Get()
        {
            return "x";
        }
    }
}