using System;

namespace UnityDI.Tests.TestDummy
{
    public class TwoConstructorsDummy : IDummy
    {
        public TwoConstructorsDummy ()
        {

        }

        public TwoConstructorsDummy (IParameter parameter)
        {

        }

        public string Get()
        {
            throw new NotImplementedException();
        }
    }
}