namespace UnityDI.Tests.TestDummy
{
    public interface IDummy
    {
        string Get();
    }
}