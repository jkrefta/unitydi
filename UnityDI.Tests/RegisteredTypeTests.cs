﻿using System;
using NUnit.Framework;
using UnityDI.Tests.TestDummy;

namespace UnityDI.Tests
{
    [TestFixture]
    public class RegisteredTypeTests
    {
        [Test]
        public void SetLifeCycleSingle_ShouldSetIsSingletonToTrue()
        {
            var registeredType = new RegisteredType(typeof(bool));
            registeredType.SetLifeCycleSingle();
            Assert.That(registeredType.IsSingleton, Is.True);
        }

        [Test]
        public void SetInstance_ShouldSetInstanceToGivenObject()
        {
            var registeredType = new RegisteredType(typeof(bool));
            registeredType.SetInstance(true);
            Assert.That(registeredType.Instance, Is.Not.Null);
            Assert.That(registeredType.Instance, Is.TypeOf<bool>());
        }

        [Test]
        public void SetInstance_WhenInstanceTypeIsDerivedFromDeclaredType_ShouldSetItToInstance()
        {
            var registeredType = new RegisteredType(typeof(IDummy));
            registeredType.SetInstance(new Dummy());
            Assert.That(registeredType.Instance, Is.TypeOf<Dummy>());
        }

        [Test]
        public void SetInstance_WhenInstanceTypeDoesNotMatchDeclaredType_ShouldThrow()
        {
            var registeredType = new RegisteredType(typeof(bool));
            Assert.That(() => registeredType.SetInstance("dupa"), Throws.InstanceOf<ArgumentException>());
        }

        [Test]
        public void SetInstance_WhenInstanceIsSingletonAndAlreadySet_ShouldThrow()
        {
            var registeredType = new RegisteredType(typeof(bool));
            registeredType.SetLifeCycleSingle();
            registeredType.SetInstance(true);
            Assert.That(() => registeredType.SetInstance(false), Throws.InstanceOf<ArgumentException>());
        }

        [Test]
        public void HasInstance_WhenNoInstanceWasSet_ShouldReturnFalse()
        {
            var registeredType = new RegisteredType(typeof(bool));
            Assert.That(registeredType.HasInstance, Is.False);
        }

        [Test]
        public void HasInstance_WhenInstanceWasSet_ShouldReturnTrue()
        {
            var registeredType = new RegisteredType(typeof(bool));
            registeredType.SetInstance(true);
            Assert.That(registeredType.HasInstance, Is.True);
        }

        [Test]
        public void SetInstance_ShouldReturnThis()
        {
            var registeredType = new RegisteredType(typeof (bool));
            var returnetValue = registeredType.SetInstance(true);
            Assert.That(returnetValue, Is.SameAs(registeredType));
        }

        [Test]
        public void SetLifeCycleSingle_ShouldReturnThis()
        {
            var registeredType = new RegisteredType(typeof(bool));
            var returnetValue = registeredType.SetLifeCycleSingle();
            Assert.That(returnetValue, Is.SameAs(registeredType));
        }

        [Test]
        public void SetDisposedManualy_ShouldReturnThis()
        {
            var registeredType = new RegisteredType(typeof(bool));
            var returnetValue = registeredType.SetDisposedManualy();
            Assert.That(returnetValue, Is.SameAs(registeredType));
        }
    }
}
