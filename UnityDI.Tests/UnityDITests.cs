﻿using System;
using NUnit.Framework;
using UnityDI.Tests.TestDummy;

namespace UnityDI.Tests
{
    [TestFixture]
    public class UnityDiTests
    {

        private UnityDi _unityDi;

        [SetUp]
        public void SetUp ()
        {
            _unityDi = new UnityDi();
        }

        [Test]
        public void Register_GivenTypeWithMoreThanOneConstructor_ShouldThrow()
        {
            Assert.That(() => _unityDi.Register<IDummy, TwoConstructorsDummy>(), Throws.TypeOf<NotSupportedException>());
        }

        [Test]
        public void Register_GivenSameTypeTwice_ShouldThrow()
        {
            _unityDi.Register<IDummy, Dummy>();
            Assert.That(() => _unityDi.Register<IDummy, Dummy>(), Throws.TypeOf<NotSupportedException>());
        }

        [Test]
        public void Unregister_GivenType_ShouldRemoveTypeFromRegister()
        {
            _unityDi.Register<IDummy, Dummy>();
            _unityDi.Unregister<IDummy>();
            Assert.That(() => _unityDi.Resolve<IDummy>(), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void Unregister_GivenType_WhenTypeWasNotRegisteredYet_ShouldThrow()
        {
            Assert.That(() => _unityDi.Unregister<IDummy>(), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void Resolve_GivenType_WhenRegisteredWithSetLifeCycleSingle_ShouldAlwaysReturnSameInstance ()
        {
            _unityDi.Register<IDummy, Dummy>().SetLifeCycleSingle();
            var firstObject = _unityDi.Resolve<IDummy>();
            var secondObject = _unityDi.Resolve<IDummy>();
            Assert.That(secondObject, Is.SameAs(firstObject));
        }

        [Test]
        public void Resolve_GivenType_ShouldReturnRegisteredImplementationOfThatType()
        {
            _unityDi.Register<IDummy, Dummy>();

            var resultObject = _unityDi.Resolve<IDummy>();

            AssertType<IDummy, Dummy>(resultObject);
        }

        [Test]
        public void Resolve_GivenTypeWithParametrizedConstructor_ShouldReturnRegisteredImplementation()
        {
            _unityDi.Register<IParameter, Parameter>();
            _unityDi.Register<IDummy, ParametrizedDummy>();

            var resultObject = _unityDi.Resolve<IDummy>();

            AssertType<IDummy, ParametrizedDummy>(resultObject);
            Assert.That(resultObject.Get(), Is.EqualTo("x"));
        }

        [Test]
        public void Resolve_GivenTypeThatIsNotRegistered_ShouldThrow()
        {
            Assert.That(() => _unityDi.Resolve<IDummy>(), Throws.InstanceOf<ArgumentException>());
        }

        private static void AssertType<TInterface, TImplementation>(object givenObject)
        {
            Assert.That(givenObject, Is.Not.Null);
            Assert.That(givenObject, Is.TypeOf<TImplementation>());
            Assert.That(givenObject, Is.InstanceOfType<TInterface>());
        }
    }

}
